import React from 'react';
import { View, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

const Header = ({title, navigation}) => (
    <View style={styles.header}>
        <TouchableWithoutFeedback onPress={()=>{
            navigation.toggleDrawer();
        }}>
            <Icon
                android="md-menu"
                ios="ios-menu"
                style={styles.headerLeftIcon}
            />
        </TouchableWithoutFeedback>
        <Text style={styles.headerTitle}>{title}</Text>
    </View>
);

const styles = StyleSheet.create({
    header:{
        height: 40,
        flexDirection:"row",
        backgroundColor: "#449",
        alignItems: "center",
        justifyContent: "space-between"
    },
    headerTitle: {
        color: "#fff",
        textAlign:"center",
        width: "90%",
        marginRight: 10,
        fontSize: 18
    },
    headerLeftIcon: {
        color: "#fff",
        marginLeft: 10
    }
})

export default Header;
