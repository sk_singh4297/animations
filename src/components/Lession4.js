import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Animated, SafeAreaView, } from 'react-native';
import Header from './Header';

class Lession4 extends Component {

    state = {
        buttonEnable: true,
        animation: new Animated.Value(25)
    }

    startAnimation = () => {
        this.setState({
            buttonEnable:false
        })
        Animated.timing(this.state.animation, {
            toValue: 50,
            duration: 1500
        }).start(() => {
            Animated.timing(this.state.animation, {
                toValue: 25,
                duration: 1500
            }).start(()=>{
                this.setState({
                    buttonEnable:true
                })
            });
        });
    }

    render () {
        console.log(this.state.animation);
        const animatedStyles = {
            width: this.state.animation,
            height: this.state.animation,
            margin:this.state.animation
        }

        const { navigation } = this.props;
        return (
            <SafeAreaView style={styles.mainContainer}>
                <Header title="Height Width" navigation={navigation}/>
                <View style={styles.container}>
                    <TouchableWithoutFeedback disabled={!this.state.buttonEnable} onPress={
                        this.startAnimation
                    }><View>
                        <View style={{flexDirection:'row'}}>
                        <Animated.View style={[styles.box, animatedStyles,{borderTopLeftRadius: this.state.animation}]} />
                        <Animated.View style={[styles.box, animatedStyles,{borderTopRightRadius: this.state.animation}]} />
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Animated.View style={[styles.box, animatedStyles,{borderBottomLeftRadius: this.state.animation}]} />
                        <Animated.View style={[styles.box, animatedStyles,{borderBottomRightRadius: this.state.animation}]} />
                    </View>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1
    },
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    box:{
        // height:150,              //default value are given via
        // width:150,               //animated.value(150) no need to provide in style
        backgroundColor:'red'
    }
})

export { Lession4 };
