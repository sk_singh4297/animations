import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Animated, SafeAreaView } from 'react-native';
import Header from './Header';

class Lession2 extends Component {

    state = {
        animationY : new Animated.Value(0),
        animationX : new Animated.Value(0),
        buttonEnable: true,
    }

    startAnimation = () => {
        this.setState({
            buttonEnable: false
        });
        Animated.timing(this.state.animationY, {
            toValue: 100,
            duration: 3000,
        }).start(() => {
            Animated.timing(this.state.animationY, {
                toValue: -100,
                duration: 3000,
            }).start(()=>{
                Animated.timing(this.state.animationY, {
                    toValue: 0,
                    duration: 3000,
                }).start(() => {
                    this.setState({
                        buttonEnable: true
                    })
                });
            });
        });
    }

    render () {
        const animatedStyles = {
             transform: [
                 {
                     translateY: this.state.animationY
                 },
                 {
                     translateX: this.state.animationX
                 }
             ]
        }

        const { navigation } = this.props;

        return (
            <SafeAreaView style={styles.mainContainer}>
                <Header title="Translation" navigation={navigation}/>
                <View style={styles.container}>
                    <TouchableWithoutFeedback disabled={!this.state.buttonEnable} onPress={
                        this.startAnimation
                    }>
                        <Animated.View style={[styles.box, animatedStyles]} />
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1
    },
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    box:{
        height:50,
        width:50,
        backgroundColor:'red'
    }
})

export { Lession2 };
