import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Animated, SafeAreaView, Text } from 'react-native';
import Header from './Header';

class Lession4H extends Component {

    state = {
        buttonEnable: true,
        animation: new Animated.Value(1)
    }

    componentDidMount() {

      }

      startAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 2,
            duration: 250,
          }).start();
      }

      render() {
        const yInterpolate = this.state.animation.interpolate({
          inputRange: [1, 2],
          outputRange: [0, -25]
        });

        const boxStyle = {
          transform: [
            { scaleY: this.state.animation },
            { translateY: yInterpolate },
            { translateX: 100}
          ]
        }

        return (
        <SafeAreaView style={{flex:1}}>
            <Header title="Scale + Translation" navigation={this.props.navigation}/>
            <View style={styles.container}>
            <TouchableOpacity onPress={this.startAnimation}><Text>Play</Text></TouchableOpacity>
                <View>
                <View style={styles.box2} />
                <Animated.View style={[styles.box, boxStyle]} />

                </View>
            </View>
        </SafeAreaView>
        );

      }
    }

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      },
      box: {
        width: 100,
        height: 100,
        backgroundColor: "tomato",
      },
      box2: {
        width: 100,
        height: 100,
        backgroundColor: "blue",
      }
    });

export { Lession4H };
