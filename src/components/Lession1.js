import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Animated, SafeAreaView } from 'react-native';
import Header from './Header';

class Lession1 extends Component {

    state = {
        buttonEnable: true,
        animation : new Animated.Value(1)
    }

    startAnimation = () => {
        this.setState({
            buttonEnable:false
        })
        Animated.timing(this.state.animation, {
            toValue: 0,
            duration: 300,
        }).start(() => {
            Animated.timing(this.state.animation, {
                toValue: 1,
                duration: 500,
            }).start(()=>{
                this.setState({
                    buttonEnable:true
                })
            });
        });
    }

    render () {
        console.log(this.props)
        const animatedStyles = {
             opacity : this.state.animation
        }
        const { navigation } = this.props;
        return (
            <SafeAreaView style={styles.mainContainer}>
                <Header title="Opacity" navigation={navigation}/>
                <View style={styles.container}>
                    <TouchableWithoutFeedback disabled={!this.state.buttonEnable} onPress={
                        this.startAnimation
                    }>
                        <Animated.View style={[styles.box, animatedStyles]} />
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1
    },
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    box:{
        height:150,
        width:150,
        backgroundColor:'red'
    }
})

export { Lession1 };
