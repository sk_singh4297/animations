import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Animated, SafeAreaView, Text } from 'react-native';
import Header from './Header';

class Lession3 extends Component {

    state = {
        buttonEnable: true,
        animation: new Animated.Value(1)
    }

    startAnimation = () => {
        this.setState({
            buttonEnable:false
        })
        Animated.timing(this.state.animation, {
            toValue: -1,
            duration: 1500
        }).start(()=>{
            Animated.timing(this.state.animation, {
                toValue: 1,
                duration: 1500
            }).start(()=>{
                this.setState({
                    buttonEnable:true
                })
            })
        });

    }

    render () {
        const animatedStyles = {
            transform: [
                {
                    scaleX: this.state.animation,
                    // scaleX:this.state.animation,
                }
            ]
        }

        const { navigation } = this.props;

        return (
            <SafeAreaView style={styles.mainContainer}>
                <Header title="Scaling" navigation={navigation}/>
                <View style={styles.container}>
                    <TouchableWithoutFeedback disabled={!this.state.buttonEnable} onPress={
                        this.startAnimation
                    }>
                        <Animated.View style={[styles.box, animatedStyles]} >
                            <Text> Hello Goku </Text>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex:1
    },
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    box:{
        height:150,
        width:150,
        backgroundColor:'red'
    }
})

export { Lession3 };
