/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import { Lession1, Lession2, Lession3, Lession4, Lession4H } from './src/components'

const Navigation = createDrawerNavigator({
  Opacity:{
    screen: Lession1,
    navigationOptions: {
      drawerLabel: 'Opacity',
    },
  },
  Translate:{
    screen: Lession2,
    navigationOptions: {
      drawerLabel: 'Translation',
    },
  },
  Scale:{
    screen: Lession3,
    navigationOptions: {
      drawerLabel: 'Scaling',
    },
  },
  HeightWidth:{
    screen: Lession4,
    navigationOptions: {
      drawerLabel: 'Height Width',
    },
  },
  CombineScaleTranslation:{
    screen: Lession4H,
    navigationOptions: {
      drawerLabel: 'Combine Scale + Translation',
    },
  }
});

export default createAppContainer(Navigation);
